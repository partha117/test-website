from build.components.templates import pages, delete_in_dir
from build.components.assets import compile_sass, copy_assets
# requires npm install -g sass

delete_in_dir()

# Builds the pages
pages()

compile_sass()

copy_assets()
