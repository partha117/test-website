import datetime
import time
from subprocess import call
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler


# Every time the "templates" folder was changed, re-run the build script
def re_run(e):
    call(['venv/bin/python', 'script/main.py'])
    print(f'Script was re-run - {datetime.datetime.now().time()}')


if __name__ == "__main__":
    path1 = 'templates'
    path2 = 'data'
    event_handler = FileSystemEventHandler()
    event_handler.on_modified = re_run
    observer = Observer()
    observer.schedule(event_handler, path1, recursive=True)
    observer.schedule(event_handler, path2, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
