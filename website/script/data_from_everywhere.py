import xml.etree.ElementTree
from slugify import slugify
from re import compile
import os
import json
import requests
from bs4 import BeautifulSoup
from build.components.data import all_objects, query_object
import xplore

# You may need to install additional requirements to use this.

xml_file = xml.etree.ElementTree.parse('data/publications/data.xml')


def ask(attr):
    ans = input(f'We couldn\'t find anything for the {attr}. Enter it here, or leave it blank: ')
    if ans == '': return None
    return ans


def can_access():
    title = pub['articles'][0]['title']

    cur = pub['articles'][0]

    urlsafe = slugify(title)

    urlsafe_tag = xml.etree.ElementTree.SubElement(pub_obj, 'urlsafe')
    urlsafe_tag.text = urlsafe

    title_tag = xml.etree.ElementTree.SubElement(pub_obj, 'title')
    title_tag.text = title

    webpage_title_tag = xml.etree.ElementTree.SubElement(pub_obj, 'webpage_title')
    webpage_title_tag.text = title

    if 'Conferences' == cur['content_type']:  # If it's a conference paper
        type_ = 'inproceedings'

        type_tag = xml.etree.ElementTree.SubElement(pub_obj, 'type')
        type_tag.text = type_

        conference_tag = xml.etree.ElementTree.SubElement(pub_obj, 'conference')
        conference_tag.text = cur['publication_title']

        booktitle_tag = xml.etree.ElementTree.SubElement(pub_obj, 'booktitle')
        booktitle_tag.text = f'Proceedings of {conference_tag.text}'

        if 'start_page' in cur and 'end_page' in cur:
            pages_tag = xml.etree.ElementTree.SubElement(pub_obj, 'pages')
            pages_tag.text = f'{cur["start_page"]}-{cur["end_page"]}'

    elif 'Journals' == cur['content_type']:
        type_ = 'article'

        type_tag = xml.etree.ElementTree.SubElement(pub_obj, 'type')
        type_tag.text = type_

        journal_tag = xml.etree.ElementTree.SubElement(pub_obj, 'journal')
        journal_tag.text = cur['publication_title']

        volume_tag = xml.etree.ElementTree.SubElement(pub_obj, 'volume')
        volume_tag.text = cur['volume']

        number_tag = xml.etree.ElementTree.SubElement(pub_obj, 'number')
        number_tag.text = cur['issue']

        pages_tag = xml.etree.ElementTree.SubElement(pub_obj, 'pages')
        pages_tag.text = f'{cur["start_page"]}-{cur["end_page"]}'

    elif cur['content_type'] == 'Early Access Articles':
        type_ = 'article'

        type_tag = xml.etree.ElementTree.SubElement(pub_obj, 'type')
        type_tag.text = type_

        journal_tag = xml.etree.ElementTree.SubElement(pub_obj, 'journal')
        journal_tag.text = cur['publication_title']

        pages_tag = xml.etree.ElementTree.SubElement(pub_obj, 'pages')
        pages_tag.text = 'To appear'

    abstract_tag = xml.etree.ElementTree.SubElement(pub_obj, 'abstract')
    abstract_tag.text = cur['abstract']

    authors_tag = xml.etree.ElementTree.SubElement(pub_obj, 'authors')

    authors_not_in_swag_tag = xml.etree.ElementTree.SubElement(pub_obj, 'authors_not_in_swag')

    author_order_tag = xml.etree.ElementTree.SubElement(pub_obj, 'author_order')

    for author in cur['authors']['authors']:
        name = author['full_name']
        obj = query_object('members', ['publication_name'], name, all_objects)[0] + query_object('members', ['name'], compile(name), all_objects)[0]
        if len(obj) != 0:  # In SWAG
            email_tag = xml.etree.ElementTree.SubElement(authors_tag, 'email')
            email_tag.text = obj[0].email.cdata
            order_tag = xml.etree.ElementTree.SubElement(author_order_tag, obj[0].urlsafe.cdata)
            order_tag.text = str(author['author_order'])
        else:  # Possibly not in SWAG
            # email = input(f'"{author["full_name"]}" user wasn\'t found in SWAG. Enter their email if they are in SWAG: ')
            # if email != '':
            #     obj = query_object('members', ['email'], compile(email), all_objects)[0]
            #     email_tag = xml.etree.ElementTree.SubElement(authors_tag, 'email')
            #     email_tag.text = obj[0].email.cdata
            #     order_tag = xml.etree.ElementTree.SubElement(author_order_tag, obj[0].urlsafe.cdata)
            #     order_tag.text = str(author['author_order'])
            # else:
            name_tag = xml.etree.ElementTree.SubElement(authors_not_in_swag_tag, 'name')
            name_tag.text = author['full_name']
            name_tag.set('id', slugify(author['full_name']))
            order_tag = xml.etree.ElementTree.SubElement(author_order_tag, slugify(author['full_name']))
            order_tag.text = str(author['author_order'])

    year_tag = xml.etree.ElementTree.SubElement(pub_obj, 'years')

    year_urlsafe_tag = xml.etree.ElementTree.SubElement(year_tag, 'urlsafe')

    year_urlsafe_tag.text = str(cur['publication_year'])

    venue_acronym = input('Enter the venue acronym: ')

    if venue_acronym != '':
        venue_tag = xml.etree.ElementTree.SubElement(pub_obj, 'venues')

        venue_acronym_tag = xml.etree.ElementTree.SubElement(venue_tag, 'acronym')

        venue_acronym_tag.text = venue_acronym


def cant_access():
    urlsafe = slugify(title)

    urlsafe_tag = xml.etree.ElementTree.SubElement(pub_obj, 'urlsafe')
    urlsafe_tag.text = urlsafe

    title_tag = xml.etree.ElementTree.SubElement(pub_obj, 'title')
    title_tag.text = title

    webpage_title_tag = xml.etree.ElementTree.SubElement(pub_obj, 'webpage_title')
    webpage_title_tag.text = title

    fields = []

    type_ = input('What type is the publication?: ')

    if 'inproceedings' == type_: # If it's a conference paper
        type_ = 'inproceedings'

        type_tag = xml.etree.ElementTree.SubElement(pub_obj, 'type')
        type_tag.text = type_

        fields.extend(['conference', 'booktitle', 'pages'])

    elif 'article' == type_:
        type_ = 'article'

        type_tag = xml.etree.ElementTree.SubElement(pub_obj, 'type')
        type_tag.text = type_

        fields.extend(['journal', 'volume', 'number', 'pages'])

    fields.extend(['abstract', 'preprint'])

    for field in fields:
        val = ask(field)
        if val is not None:
            current_tag = xml.etree.ElementTree.SubElement(pub_obj, field)
            current_tag.text = val

    authors_tag = xml.etree.ElementTree.SubElement(pub_obj, 'authors')

    authors_not_in_swag_tag = xml.etree.ElementTree.SubElement(pub_obj, 'authors_not_in_swag')

    author_order_tag = xml.etree.ElementTree.SubElement(pub_obj, 'author_order')

    authors_val = input('Enter a comma-separated list of authors\'s emails: ')

    for auth in authors_val.split(','):
        new_author = xml.etree.ElementTree.SubElement(authors_tag, 'email')
        new_author.text = auth

    authors_not_in_swag_val = input('Enter a comma-separated list of authors\'s not in SWAG\'s name: ')

    if authors_not_in_swag_val != '':
        for auth in authors_not_in_swag_val.split(','):
            new_author = xml.etree.ElementTree.SubElement(authors_not_in_swag_tag, 'name')
            new_author.set('id', slugify(auth))
            new_author.text = auth

    for auth in authors_val.split(','):
        current_author = query_object('members', ['email'], auth, all_objects)[0][0]
        order = input(f'Enter the order of the author "{current_author.name.cdata}": ')
        author_order = xml.etree.ElementTree.SubElement(author_order_tag, current_author.urlsafe.cdata)
        author_order.text = order

    if authors_not_in_swag_val != '':
        for auth in authors_not_in_swag_val.split(','):
            order = input(f'Enter the order of the author "{auth}": ')
            author_order = xml.etree.ElementTree.SubElement(author_order_tag, slugify(auth))
            author_order.text = order

    year_tag = xml.etree.ElementTree.SubElement(pub_obj, 'years')

    year_val = ask('pub_year')

    year_urlsafe_tag = xml.etree.ElementTree.SubElement(year_tag, 'urlsafe')

    year_urlsafe_tag.text = year_val

    venue_acronym = input('Enter the venue acronym: ')

    if venue_acronym != '':
        venue_tag = xml.etree.ElementTree.SubElement(pub_obj, 'venues')

        venue_acronym_tag = xml.etree.ElementTree.SubElement(venue_tag, 'acronym')

        venue_acronym_tag.text = venue_acronym


def acm_access():
    soup = BeautifulSoup(requests.get(acm_link).text, features='lxml')

    title = soup.select('title')[0].string.split(' | ')[0]

    urlsafe_tag = xml.etree.ElementTree.SubElement(pub_obj, 'urlsafe')
    urlsafe_tag.text = slugify(title)

    title_tag = xml.etree.ElementTree.SubElement(pub_obj, 'title')
    title_tag.text = title

    webpage_title_tag = xml.etree.ElementTree.SubElement(pub_obj, 'webpage_title')
    webpage_title_tag.text = title

    try:
        journal = soup.select('[name="citation_journal_title"]')[0]['content']
        type_ = 'article'
    except:
        type_ = 'inproceedings'

    type_tag = xml.etree.ElementTree.SubElement(pub_obj, 'type')
    type_tag.text = type_

    if type_ == 'article':
        journal_tag = xml.etree.ElementTree.SubElement(pub_obj, 'journal')
        journal_tag.text = journal

        volume, issue = map(lambda x: x.split()[1], soup.select('.serial-info')[0].string.strip().split(', '))

        volume_tag = xml.etree.ElementTree.SubElement(pub_obj, 'volume')
        volume_tag.text = volume

        number_tag = xml.etree.ElementTree.SubElement(pub_obj, 'number')
        number_tag.text = issue

    elif type_ == 'inproceedings':
        conference = soup.select('title')[0].string.split(' | ')[1][19:]
        booktitle = soup.select('title')[0].string.split(' | ')[1]

        conference_tag = xml.etree.ElementTree.SubElement(pub_obj, 'conference')
        conference_tag.text = conference

        booktitle_tag = xml.etree.ElementTree.SubElement(pub_obj, 'booktitle')
        booktitle_tag.text = booktitle

    try:
        pages = soup.select('.epub-section__pagerange')[0].string.strip().split()[1]

        pages_tag = xml.etree.ElementTree.SubElement(pub_obj, 'pages')
        pages_tag.text = pages
    except:
        pass

    abstract = soup.select('.abstractInFull')[0].p.text.strip()

    abstract_tag = xml.etree.ElementTree.SubElement(pub_obj, 'abstract')
    abstract_tag.text = abstract

    authors_tags = soup.select('.loa__author-name')
    authors = []

    authors_tag = xml.etree.ElementTree.SubElement(pub_obj, 'authors')

    authors_not_in_swag_tag = xml.etree.ElementTree.SubElement(pub_obj, 'authors_not_in_swag')

    author_order_tag = xml.etree.ElementTree.SubElement(pub_obj, 'author_order')

    for author in authors_tags[:len(authors_tags) // 2]:
        author.img.decompose()
        authors.append(author.span.string)

    for i, author in enumerate(authors):
        obj = query_object('members', ['publication_name'], f'{author}', all_objects)[0] + query_object('members', ['name'], compile(f'(?i){author}'), all_objects)[0]
        if len(obj) != 0:  # In SWAG
            email_tag = xml.etree.ElementTree.SubElement(authors_tag, 'email')
            email_tag.text = obj[0].email.cdata
            order_tag = xml.etree.ElementTree.SubElement(author_order_tag, obj[0].urlsafe.cdata)
            order_tag.text = str(i + 1)
        else:  # Possibly not in SWAG
            # email = input(f'"{author}" user wasn\'t found in SWAG. Enter their email if they are in SWAG: ')
            # if email != '':
            #     obj = query_object('members', ['email'], compile(f'{email}'), all_objects)[0]
            #     email_tag = xml.etree.ElementTree.SubElement(authors_tag, 'email')
            #     email_tag.text = obj[0].email.cdata
            #     order_tag = xml.etree.ElementTree.SubElement(author_order_tag, obj[0].urlsafe.cdata)
            #     order_tag.text = str(i + 1)
            # else:
            name_tag = xml.etree.ElementTree.SubElement(authors_not_in_swag_tag, 'name')
            name_tag.text = author
            name_tag.set('id', slugify(author))
            order_tag = xml.etree.ElementTree.SubElement(author_order_tag, slugify(author))
            order_tag.text = str(i + 1)


    year = soup.select('.CitationCoverDate')[0].string.strip().split()[-1]

    year_tag = xml.etree.ElementTree.SubElement(pub_obj, 'years')

    year_urlsafe_tag = xml.etree.ElementTree.SubElement(year_tag, 'urlsafe')

    year_urlsafe_tag.text = str(year)

    venue_acronym = input('Enter the venue acronym: ')

    if venue_acronym != '':
        venue_tag = xml.etree.ElementTree.SubElement(pub_obj, 'venues')

        venue_acronym_tag = xml.etree.ElementTree.SubElement(venue_tag, 'acronym')

        venue_acronym_tag.text = venue_acronym


while True:

    title = input('Enter the title: ')
    if title == 'q': break

    pub_obj = xml.etree.ElementTree.SubElement(xml_file.getroot(), 'data')

    search_query = xplore.xploreapi.XPLORE(os.environ['IEEE_KEY'])
    search_query.articleTitle(title)

    pub = json.loads(search_query.callAPI())

    if pub['total_records'] == 0:
        ieee_number = input('IEEE can\'t be found... Enter article #: ')
        if ieee_number != '':
            search_query.articleNumber(ieee_number)
            pub = json.loads(search_query.callAPI())
            can_access()
        else:
            acm_link = input('Enter the ACM Link: ')
            if acm_link == '':
                cant_access()
            else:
                acm_access()
    else:
        can_access()

    xml_file.write('data/publications/data.xml')

    print('The publication has been added!')

    # sleep(12)
