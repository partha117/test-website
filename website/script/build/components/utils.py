from operator import attrgetter
from pybtex.database import BibliographyData, Entry
from .data import get_safe_tag_name


def jinja_sort(elem, key=None, reverse=False) -> any:
    if key is None:
        return sorted(elem, reverse=reverse)
    return sorted(elem, key=eval(key), reverse=reverse)


def jinja_filter(iterable, func):
    return list(filter(eval(func), iterable))


def xml_getattr(obj, attr, default=None) -> any:
    try:
        if attrgetter(attr)(obj) is not None:
            return attrgetter(attr)(obj)
        return default
    except AttributeError:
        return default


def get_bibtex(publication) -> str:
    data = {}

    authors = get_publication_author_names(publication)

    # Generate an entry name with first author's name without spaces, year, and first letters of the title
    entry_name = f'{authors[0].lower().replace(" ", "")}{publication.years[0].urlsafe.cdata}{"".join([s[0].lower() for s in publication.title.cdata.split()])}'

    # Add entries to the BibTeX data if they exist
    data['author'] = ' and '.join(get_publication_author_names(publication))
    if publication.title is not None:
        data['title'] = publication.title.cdata
    if publication.years is not None:
        data['year'] = publication.years[0].urlsafe.cdata
    if publication.pages is not None:
        data['pages'] = publication.pages.cdata

    if publication.type.cdata == 'article':
        entry_type = 'article'
        if publication.journal is not None:
            data['journal'] = publication.journal.cdata
        if publication.volume is not None:
            data['volume'] = publication.volume.cdata
        if publication.number is not None:
            data['number'] = publication.number.cdata

    elif publication.type.cdata == 'inproceedings':
        entry_type = 'inproceedings'
        if publication.booktitle is not None:
            data['booktitle'] = publication.booktitle.cdata
        elif publication.conference is not None:
            data['booktitle'] = f'Proc. of {publication.conference.cdata}'

    elif publication.type.cdata == 'phdthesis' or publication.type.cdata == 'mastersthesis':
        entry_type = publication.type.cdata
        if publication.school is not None:
            data['school'] = publication.school.cdata
        if publication.address is not None:
            data['address'] = publication.address.cdata

    # Create the BibTeX object
    bibtex = BibliographyData({
        entry_name: Entry(entry_type, data)
    })

    # Return the string representation of the BibTeX object
    return bibtex.to_string('bibtex')


def get_plain_citation(publication) -> str:
    citation = ''

    pub_author_names = get_publication_author_names(publication)
    if pub_author_names is not None:
        if len(pub_author_names) == 1:
            citation += f'{pub_author_names[0]}, '
        elif len(pub_author_names) == 2:
            citation += f'{pub_author_names[0]} and {pub_author_names[1]}, '
        else:
            citation += ', '.join(pub_author_names[:-1])
            citation += f', and {pub_author_names[-1]}, '
    if publication.title is not None:
        citation += f'"{publication.title.cdata}," '
    if publication.type == 'inproceedings' and publication.conference is not None:
        citation += f'{publication.conference.cdata}, '
    elif publication.type == 'article' and publication.journal is not None:
        citation += f'{publication.journal.cdata}, '
    elif (publication.type == 'mastersthesis' or publication.type == 'phdthesis') and publication.school is not None:
        citation += f'{publication.school.cdata}, '
    if publication.year is not None:
        citation += f'{publication.year.cdata}, '
    if publication.pages is not None:
        citation += f'pp. {publication.pages.cdata}, '

    citation = citation[:-2]  # Remove trailing commas and space

    return citation

# Citation: full names (comma-separated last one and), "Title," Location (conference or journal), Year, Pages -->


def get_venue(publication) -> str:  # Get the venue to be shown on the publication widget
    if publication.type.cdata == 'article':
        ret = publication.journal.cdata
        if publication.volume is not None:
            ret += f', Vol. {publication.volume.cdata}'
        if publication.number is not None:
            ret += f', No. {publication.number.cdata}'
        if publication.pages is not None:
            ret += f', pp. {publication.pages.cdata}'
        if publication.years is not None:
            ret += f', {publication.years[0].urlsafe.cdata}'
        return ret
    elif publication.type.cdata == 'inproceedings':
        ret = publication.conference.cdata
        if publication.pages is not None:
            ret += f', pp. {publication.pages.cdata}'
        if publication.years is not None:
            ret += f', {publication.years[0].urlsafe.cdata}'
        return ret
    elif publication.type.cdata == 'phdthesis' or publication.type.cdata == 'mastersthesis':
        ret = publication.school.cdata
        if publication.pages is not None:
            ret += f', pp. {publication.pages.cdata}'
        if publication.years is not None:
            ret += f', {publication.years[0].urlsafe.cdata}'
        return ret


def get_publication_authors(publication) -> list:
    """
    Returns a list of tuples, containing either an untangle.Element object (if the author is in SWAG)
    or an author name, and a bool representing whether the author is in SWAG or not
    """
    authors_in_swag = [(author, True, getattr(publication.author_order, get_safe_tag_name(author.urlsafe.cdata)).cdata) for author in xml_getattr(publication, 'authors', default=[])]
    authors_not_in_swag = [(author_name, False, getattr(publication.author_order, get_safe_tag_name(author_name['id'])).cdata) for author_name in xml_getattr(publication, 'authors_not_in_swag.name', default=[])]
    return [(a[0], a[1]) for a in sorted(authors_in_swag + authors_not_in_swag, key=lambda x: int(x[2]))]


def get_publication_author_names(publication):
    """
    Returns only the names of all of the authors in a publication. Does not return any untangle.Element objects.
    """
    authors_in_swag = [(author.publication_name.cdata, getattr(publication.author_order, get_safe_tag_name(author.urlsafe.cdata)).cdata) for author in xml_getattr(publication, 'authors', default=[])]
    authors_not_in_swag = [(author_name.cdata, getattr(publication.author_order, get_safe_tag_name(author_name['id'])).cdata) for author_name in xml_getattr(publication, 'authors_not_in_swag.name', default=[])]
    return [a[0] for a in sorted(authors_in_swag + authors_not_in_swag, key=lambda x: int(x[1]))]
