import subprocess
from shutil import copytree
import glob
from .templates import DIRECTORY
from tqdm import tqdm


# Compile Sass
def compile_sass() -> None:
    with tqdm(total=1, desc='Compiling Sass') as pbar:
        subprocess.call(['sass', f'templates/assets/scss:{DIRECTORY}/assets/css'])
        pbar.update(1)


# Copy all assets from templates/assets to {DIRECTORY}/assets
def copy_assets() -> None:
    for folder in tqdm(glob.glob('templates/assets/*/'), desc='Transferring assets'):
        folder_name = folder[:-1].split('/')[-1]
        if folder_name != 'scss':  # We are compiling the scss separately
            copytree(folder, f'{DIRECTORY}/assets/{folder_name}', dirs_exist_ok=True)
