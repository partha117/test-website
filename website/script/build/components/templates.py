import re
import os
import shutil
from pathlib import Path
from tqdm import tqdm
from build import jinja_env
from . import utils
from .data import get_pages, all_objects, all_pages, query_object, get_safe_tag_name

# Change prefix incase of repo chnage
PREFIX = ''
DIRECTORY = 'www'


def delete_in_dir() -> None:
    ignore = ['.git', '.gitattributes']
    for fd in tqdm(os.listdir(f'{DIRECTORY}'), desc='Cleaning up old built files'):
        if fd in ignore: continue
        if os.path.isdir(f'{DIRECTORY}/{fd}'):
            shutil.rmtree(f'{DIRECTORY}/{fd}')
        else:
            os.remove(f'{DIRECTORY}/{fd}')


def pages() -> None:
    pages = get_pages()

    navbar_data = list(filter(lambda x: x.navbar.cdata == 'true', all_pages.page))

    index = jinja_env.get_template('index.html')

    with open(f'{DIRECTORY}/index.html', 'w+') as f:
        f.write(index.render(navbar=navbar_data, query_object=query_object, current_page='Home', all_objects=all_objects, utils=utils, regex=re, PREFIX=PREFIX))

    # For each page
    pages_progress = tqdm(pages.page)
    for page in pages_progress:
        pages_progress.set_description(f'Creating "{page.name.cdata}" templates')
        if page.type == 'static':
            template = jinja_env.get_template(f'{page.urlsafe}/index.html')
            with open(f'{DIRECTORY}/{page.urlsafe}.html', 'w+') as f:
                f.write(template.render(navbar=navbar_data, current_page=page.name.cdata, utils=utils, get_safe_tag_name=get_safe_tag_name, PREFIX=PREFIX))

        elif page.type == 'all' or page.type == 'dynamic':
            all_template = jinja_env.get_template(f'{page.urlsafe}/all.html')

            with open(f'{DIRECTORY}/{page.urlsafe}.html', 'w+') as f:
                f.write(all_template.render(navbar=navbar_data, current_page=page.name.cdata, query_object=query_object, regex=re, all_objects=all_objects, utils=utils, get_safe_tag_name=get_safe_tag_name, PREFIX=PREFIX, **all_objects))
            
        if page.type == 'specific' or page.type == 'dynamic':
            specific_template = jinja_env.get_template(f'{page.urlsafe}/specific.html')

            Path(f'{DIRECTORY}/{page.urlsafe}').mkdir(parents=True, exist_ok=True)

            # For each specific data object
            for specific in all_objects[get_safe_tag_name(page.urlsafe.cdata)]:
                with open(f'{DIRECTORY}/{page.urlsafe}/{specific.urlsafe}.html', 'w+') as f:
                    f.write(specific_template.render(navbar=navbar_data, data=specific, current_page=specific.webpage_title.cdata, query_object=query_object, regex=re, all_objects=all_objects, utils=utils, get_safe_tag_name=get_safe_tag_name, PREFIX=PREFIX, **all_objects))
    pages_progress.close()
