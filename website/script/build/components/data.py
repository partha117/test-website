import re
from tqdm import tqdm
import types
from operator import attrgetter
import untangle


def get_safe_tag_name(name):
    return name.replace('-', '_').replace('.', '_').replace(':', '_')


def get_pages():
    """
    Parses and returns a Python object of all of the pages
    """
    with open('data/pages.xml') as f:
        return untangle.parse(f).pages


def strip_data(data):
    """
    Strips the outer data tag from all of the page data objects passed
    """
    return {a: data[a].data for a in data}


def query_object(page: str, attr_name: list, value: str, all_obj: dict) -> tuple:
    """
    Returns a list of XML data elements such that the data's attribute is equal to a specified value
    Attributes can be nested using a list
    Uses a modified BFS in the case that there are multiple nested elements with the same tag name
    """

    if not attr_name:
        raise IndexError('No XML object attributes provided')

    ret = []
    ret_indices = []
    for i, d in enumerate(all_obj[page]):  # Iterate every data field

        q = []
        for e in d.children:
            if e._name == attr_name[0]:  # Add the current element if the tag is equal to the first requested attribute
                q.append((e, 0))  # Store the element and the current attribute level

        while q:
            cur, lvl = q.pop(
                0)  # cur represents the current XML element, while lvl represents the current level of attr_name (index)
            if lvl == len(attr_name) - 1:  # Final attribute in nesting
                if cur.cdata is None: break
                if value == cur.cdata:  # If the element has the requested query value, add the data element
                    ret.append(d)
                    ret_indices.append(i)
                    break
            elif hasattr(cur, attr_name[lvl + 1]):  # If the element has the child
                for child in getattr(cur, attr_name[lvl + 1]):  # For each element with that tag name
                    q.append((child, lvl + 1))

    return ret, ret_indices  # Returns objects and the indices of the object that satisfy the criteria


def get_objects() -> dict:
    """
    Returns a dictionary of untangle Element objects with relationships set. 
    Looks through each field, and if it's a relationship, uses setattr to set 
    the data object(s) that it has a relation to as an attribute
    """
    all_pages = get_pages()
    fields = {}
    ret = {}
    relation_has_been_set = {}

    for page in all_pages.page:
        if page.type.cdata == 'static' or page.type.cdata == 'index':
            continue
        try:
            with open(f'data/{page.urlsafe.cdata}/fields.xml') as f:
                fields[get_safe_tag_name(page.urlsafe.cdata)] = untangle.parse(f).fields
            with open(f'data/{page.urlsafe.cdata}/data.xml') as f:
                ret[get_safe_tag_name(page.urlsafe.cdata)] = untangle.parse(
                    f).data  # The new objects (with relationships defined)

        except OSError:
            pass  # Temporary solution

    for k in tqdm(fields, desc='Assigning default values'):
        for elem in fields[k].field:
            if elem.type == 'one' and elem.default is not None:
                default_value = elem.default.cdata
                tag_name = elem.name.cdata
                for i in range(len(ret[k].data)):  # For each data object
                    if getattr(ret[k].data[i], tag_name) is None:
                        new_element = untangle.Element(tag_name, dict())
                        if default_value[
                           :5] == 'self.':  # if the default value is an attribute that exists already in the data object (self.*)
                            new_element.cdata = (attrgetter(default_value[5:])(ret[k].data[i])).cdata
                        else:
                            new_element.cdata = default_value
                        # setattr(ret[k].data[i], tag_name, new_element)
                        ret[k].data[i].add_child(new_element)

    # For each field, set the relationship attribute to the object that it has a relation to
    for k in tqdm(fields, desc='Defining relationships'):
        for elem in fields[k].field:  # For each field
            if elem.type == 'relation':  # The current field is a relation
                relation_page = elem.relation.cdata  # Holds the page that the current relation has a relation to
                relation_name = elem.name.cdata
                relation_reverse_attribute = elem.reverse.cdata
                for i, d in enumerate(ret[k].data):  # For each data object
                    if getattr(d, relation_name) is None:
                        continue
                    for child in getattr(d,
                                         relation_name).children:  # Get all children of the data being provided inside of a relational element (like "email" inside the relational element "publication.author")
                        relation_attribute = child._name
                        relation_value = child.cdata
                        relation_objs, relation_obj_indices = query_object(relation_page,
                                                                           [relation_attribute],
                                                                           relation_value, strip_data(ret))
                        if not relation_has_been_set.get((ret[k].data[i].string_representation, relation_name),
                                                         False):  # Has a relation not been set before for this attribute and data object
                            setattr(ret[k].data[i], relation_name, relation_objs)
                            relation_has_been_set[(ret[k].data[i].string_representation, relation_name)] = True
                        else:
                            setattr(ret[k].data[i], relation_name,
                                    list(getattr(ret[k].data[i], relation_name)) + relation_objs)
                        for oi in relation_obj_indices:
                            if not relation_has_been_set.get((ret[relation_page].data[oi].string_representation, k),
                                                             False):  # Has a relation not been set before for this attribute and data object
                                setattr(ret[relation_page].data[oi], relation_reverse_attribute, [ret[k].data[i]])
                                relation_has_been_set[(ret[relation_page].data[oi].string_representation, k)] = True
                            else:
                                setattr(ret[relation_page].data[oi], relation_reverse_attribute,
                                        list(getattr(ret[relation_page].data[oi], relation_reverse_attribute)) + [
                                            ret[k].data[i]])
    stripped_data = strip_data(ret)
    return {get_safe_tag_name(a): stripped_data[a] for a in stripped_data}


all_objects = get_objects()

all_pages = get_pages()
