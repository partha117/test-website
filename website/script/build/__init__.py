from jinja2 import Environment, FileSystemLoader, select_autoescape
from build.components.utils import jinja_sort, jinja_filter, xml_getattr

# Defines Jinja environment
jinja_env = Environment(
    loader=FileSystemLoader("templates"),
    autoescape=select_autoescape()
)

jinja_env.filters['custom_sort'] = jinja_sort

jinja_env.filters['custom_filter'] = jinja_filter

jinja_env.filters['custom_attr'] = xml_getattr
