# Data

## All pages

The `data` directory contains the data (stored in XML files) that is then run through the script then passed to the templates. Data is divided into pages, which are defined in the `pages.xml` file. The `pages.xml` file has an outer element called `pages`. Each page should have an element in the `pages.xml` file that looks as follows:

The script first converts the XML data into Python objects using a fork of the `Untangle` Python library, which can be found [here](https://github.com/vishnupsatish/detangle). Then, it assigns default values and creates relationships. 

```xml
<page>
  <name> </name>
  <urlsafe> </urlsafe>
  <type> </type>
  <navbar> </navbar>
</page>
```

- `<name>` — the name of the page.
- `<urlsafe>` — the URL-safe name of the page, which will be used in the URL of the page, such as `{urlsafe}.html`. Also used as the directory name for the page's data.
- `<type>` — type of the page. The types are
  - `index` — the home page
  - `dynamic` — data is stored in XML files, then passed to the two templates (all and specific)
  - `all` — data is stored in XML files, then passed to only one template (all)
  - `specific` — data is stored in XML files, then passed to only one template (specific)
  - `static` — a static HTML file, and no data stored in XML files
- `<navbar>` — whether the page will be displayed in the navbar or not. The values are
  - `true`
  - `false`
    
## Each page

The `data` directory also contains the data for each specific data page (that has a type that is dynamic, all, or specific). Inside each directory is two files, `fields.xml` and `data.xml`.

### Fields

`fields.xml` contains an outer element called `fields`, and each inner element should look as follows.

```xml
<field>
  <name> </name>
  <type> </type>
</field>
```

- `<name>` — the name of the field
- `<type>` — the type of the field. The possible values are
  - `one` — a single value
  - `custom` — a custom nestable element with an unlimited number of sub-elements inside
  - `relation` — a relationship field, which has a relation to other data objects either in the same page, or a different page
- `<default>` (if `<type>` is `one`) (optional) — a default value for the field if the value is not given
  - if the default value begins with `self.`, then the default value will be assigned to another value within the same data element. If the other value is not found, undefined behaviour will occur.
- `<relation>` (if `<type>` is `relation`) (required) — the page to define the relationship with.
- `<reverse>` (if `<type>` is `relation`) (required) — the reverse attribute name. Since relationships are only defined one-way, `reverse` determines the attribute name of the data pieces that are a part of the current page, from the `relation` page.

There are two fields that are required for the data of most pages, and these are
- `urlsafe` — the filename used for creating each specific page
- `webpage_title` — the value of the `<title>` HTML element for that data's specific page
These fields are required for the data of all pages that do not have a type of "all".

### Data

`data.xml` contains an outer element called `data`, and each inner element should look as follows

```xml
<data>
  <urlsafe> </urlsafe>
  <webpage_title> </webpage_title>
  ...
</data>
```

The field names in `fields.xml` are the tag names for each piece of data in `data.xml`. Data for fields with type `one` are to be put in between the tags. There should be no nesting inside of tags whose fields have type `one`. Elements whose fields are of the type `relation` should have sub-elements the tag whose tag names correspond to any field name with type `one` in the destination page. Elements whose fields are of the type `custom` can have any data and any number of sub-elements inside.

## Example

`pages.xml`

```xml
<pages>
  <page>
    <name>Restaurants</name>
    <urlsafe>restaurants</urlsafe>
    <type>dynamic</type>
    <navbar>true</navbar>
  </page>
  <page>
    <name>Food</name>
    <urlsafe>food</urlsafe>
    <type>dynamic</type>
    <navbar>true</navbar>
  </page>
</pages>
```

`restaurants/fields.xml`

```xml
<fields>
  <field>
    <name>urlsafe</name>
    <type>one</type>
  </field>
  <field>
    <name>webpage_title</name>
    <type>one</type>
    <default>self.name</default>
  </field>
  <field>
    <name>name</name>
    <type>one</type>
  </field>
  <field>
    <name>type</name>
    <type>one</type>
  </field>
  <field>
    <name>food</name>
    <type>relation</type>
    <relation>food</relation>
    <reverse>restaurants</reverse>
  </field>
</fields>
```

`food/fields.xml`

```xml
<fields>
  <field>
    <name>urlsafe</name>
    <type>one</type>
  </field>
  <field>
    <name>webpage_title</name>
    <type>one</type>
    <default>self.name</default>
  </field>
  <field>
    <name>name</name>
    <type>one</type>
  </field>
  <field>
    <name>countries</name>
    <type>custom</type>
  </field>
</fields>
```

`restaurants/data.xml`

```xml
<data>
  <data>
    <urlsafe>olive-garden</urlsafe>
    <name>Olive Garden</name>
    <type>Casual Dining</type>
    <food>
      <name>Pasta</name>
      <name>Sandwich</name>
    </food>
  </data>
  <data>
    <urlsafe>mcdonalds</urlsafe>
    <name>McDonald's</name>
    <type>Fast Food</type>
    <food>
      <name>Sandwich</name>
    </food>
  </data>
</data>
```

`food/data.xml`

```xml
<data>
  <data>
    <urlsafe>pasta</urlsafe>
    <name>Pasta</name>
    <countries>
      <country>
        <name>China</name>
        <continent>Asia</continent>
      </country>
      <country>
        <name>Italy</name>
        <continent>Europe</continent>
      </country>
    </countries>
  </data>
  <data>
    <urlsafe>sandwich</urlsafe>
    <name>Sandwich</name>
    <countries>
      <country>
        <name>Italy</name>
        <continent>Europe</continent>
      </country>
      <country>
        <name>United Kingdom</name>
        <continent>Europe</continent>
      </country>
    </countries>
  </data>
</data>
```

### Restaurants

The restaurants page has its fields outlined in `restaurants/fields.xml`, and data in `restaurants/data.xml`. There are two pieces of data for `restaurants`, and they represent McDonald's and Olive Garden. Each restaurants' urlsafe value, name, and type are given. 

### Food

The food page has its fields outlined in `food/fields.xml`, and data in `food/data.xml`. There are two pieces of data for `food`, and they represent pasta and sandwich. Each restaurants' urlsafe value, name, and countries are given. Note that `countries` is a custom field, meaning any number of sub-elements can be nested inside it. 

### Data

Since the data is converted to a Python object when it is passed to the templates, we can access the data as follows. 

Say we wanted to access the first food's second country's continent, we would do the following (where data represents the root element of the `food/data.xml` file).

```python
data.data[0].countries.country[1].continent.cdata
```

We need the .cdata to access the text inside the element.

### Relation

In `restaurant`'s fields, a relational field related to the `food` page is defined. This means that accessing the `food` attribute of a restaurant's data returns a list of the Python objects whose attribute (given by the tag name in the relational element) is equal to the value of the element.

For example, the second restaurant, McDonald's, has a relationship defined for the food whose name is equal to `Sandwich`. This means, doing the following (where data represents the root element of the `restaurants/data.xml` file)

```python
data.data[1].food[0].countries.country[0].continent.cdata
```

would return `Europe`.


## Restrictions

Note that pages cannot be named the following.
- `navbar`
- `data`
- `current_page`
- `query_object`
- `regex`
- `all_objects`
- `utils`
- `get_safe_tag_name`

