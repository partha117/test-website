# Script

The `script` directory contains Python files (arranged in a module) that serve different purposes, such as preparing the data, passing the data to the templates, and more.

The goal of the script is to first prepare the data, then pass the data to the templates, and build the (now-static) templates to the `dist` directory.

The directory structure (of the relevant files) is outlined below.

```
|-- script
|   |-- main.py
|   |-- watch.py
|   `-- build
|       |-- __init__.py
|       `-- components
|           |-- __init__.py
|           |-- assets.py
|           |-- data.py
|           |-- templates.py
|           `-- utils.py
```

### `main.py`

Runs the functions from the `components` module.

### `build/__init__.py`

Creates the Jinja (templating language) environment and initializes the custom Jinja filters.

### `build/components/__init__.py`

An empty file used to initialize a Python module.

### `build/components/assets.py`

Handles the assets (images, CSS, JavaScript, etc.). Compiles the Sass to CSS using the Sass CLI and transfers the other assets to the `dist` directory.

### `build/components/data.py`

Handles the data generation and preparation.

`query_object(page: str, attr_name: list, value: str, all_obj: dict) -> tuple`

Queries an XML data object such that the (possibly) nested `attr_name` == `value`. Returns a tuple of the objects and the indices that match the requirements.

`get_objects() -> dict`

Returns a dictionary with the key being the `urlsafe` value of the page, and the value being the XML object with relationships and default values assigned.

### `build/components/templates.py`

Builds the templates with the data provided by `get_objects()` being passed as keyword arguments. Any function or variable declared in `build/components/utils.py` is passed as `utils.<name>`.

### `build/components/utils.py`

Utility functions which can be created by the user.A ny function or variable declared in `build/components/utils.py` is passed to each template as `utils.<name>`.
