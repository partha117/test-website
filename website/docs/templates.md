# Templates

Each page (defined in `pages.xml`) has a directory in `templates`.

In `pages.xml`, each page has a type. (Either `index`, `dynamic`, `all`, `specific`, or `static`).

In `script/build/components/templates.py`, two variables, `PREFIX` and `DIRECTORY` should be changed to match whichever directory the built site is saved to. All `href` and `src` attributes that do not have a possibility to point to external sites should be prefixed with `{{ PREFIX }}`.

### All types

Regardless of the type, every template is passed the dictionary returned by `get_objects()` as a keyword argument. Every template is also passed the `utils` module, and a few custom Jinja filters such as `custom_sort` and `custom_filter`, which are defined in `script/build/components/utils.py`.

### Type = `index`

This is the index page, and is defined in `templates/index.html`.

### Type = `dynamic`

There are two template files in `templates/<urlsafe of page>`, `all.html` and `specific.html`. `all.html` is created once as `<page urlsafe>.html`, and `specific.html` is created once for every data object in the XML file as the `<page urlsafe>/<data urlsafe>.html`. The specific data object is passed as `data`, so you would do, for example, to access a member's name in their specific page, `data.title.cdata`. 

### Type = `all`

Only the `all.html` template is considered.

### Type = `specific`

Only the `specific.html` template is considered.

### Type = `static`

No XML files for data are considered, only the template in `<page urlsafe>/index.html` is considered. You can use Jinja in this page, though.
