## Software Architecture Group Website

The Software Architecture Group website is divided into different directories that serve different purposes. Each directory has its own documentation.

1. [Data](data.md)
2. [Script](script.md)
3. [Templates](templates.md)

Useful documentation about the current setup (such as how to add a user) can be found below.

- [Add a user](how-to/add-a-user.md)
- [Add a publication](how-to/add-a-publication.md)
