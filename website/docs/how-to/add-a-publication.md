# How to add a publication

In `data/publications/data.xml` create a new XML element inside the root element called `<data>`. Inside this, you can go to `data/publications/fields.xml` to see what tags and information you need to add. To make it easier, you may also copy another publication's data object and change the values.

### Authors

There are two fields available to add authors, `authors` and `authors_not_in_swag`. Author order must also be specified. 

`authors` is a relational element, meaning you can use any field in members to define a relationship. Using `email` is preferred though. 

`authors_not_in_swag` is a custom element. Inside it, add elements called `name`, each having an XML attribute `id`. The value of `id` should be such that it is a valid XML tag name. The value of `name` should be the author's name.

To define author order, the `author_order` element is used. For each author in `authors` and `author_not_in_swag`, an element in `author_order` must be created. If it's an author from SWAG: make the tag name the urlsafe value of the author who has been assigned to the relationship. If an author not from SWAG, the tag name must be the value of the `id` attribute from the `authors_not_in_swag` element. Take the below example.

```xml
 <authors>
    <email>mei.nagappan@uwaterloo.ca</email> <!-- urlsafe value is mei-nagappan -->
</authors>
<authors_not_in_swag>
    <name id="tse-hsun-peter-chen">Tse-Hsun Peter Chen</name>
    <name id="weiyi-shang">Weiyi Shang</name>
    <name id="ahmed-e-hassan">Ahmed E. Hassan</name>
    <name id="stephen-w-thomas">Stephen W. Thomas</name>
</authors_not_in_swag>
<author_order>
    <mei-nagappan>3</mei-nagappan>
    <tse-hsun-peter-chen>1</tse-hsun-peter-chen>
    <weiyi-shang>2</weiyi-shang>
    <ahmed-e-hassan>4</ahmed-e-hassan>
    <stephen-w-thomas>5</stephen-w-thomas>
</author_order>
```

Here, Mei Nagappan is the only person who is from SWAG that has authored this paper. All the other authors are not in SWAG. For Mei Nagappan, the urlsafe value of his element in the `members` page must be used as the tag name in `author_order`. For all the other authors, the `id` XML attribute of their respective element in `authors_not_in_swag` must be used as the tag name. The author order of this publication would then be:

Tse-Hsun Peter Chen, Weiyi Shang, Meiyappan Nagappan, Ahmed E. Hassan, Stephen W. Thomas.

### Required fields

The way it works currently, the following fields are required.

- `urlsafe`
- `webpage_title`
- `title`
- `authors` or `authors_not_in_swag` or both
- `years`
- `type`: either `article`, `inproceedings`, `mastersthesis`, or `phdthesis`
