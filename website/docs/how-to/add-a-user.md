# How to add a user

In `data/members/data.xml` create a new XML element inside the root element called `<data>`. Inside this, you can go to `data/members/fields.xml` to see what tags and information you need to add. To make it easier, you may also copy another member's data object and change the values.

### Required fields

The way it works currently, the following fields are required.

- `urlsafe`
- `webpage_title`
- `name`
- `type`: either Professor, Post-doc, Visitors, Staff, Students, Interns, Alumni
- `headline`: if the type is Student, be sure to normalize the `headline` to PhD Student or MMath Student (if a master's student) or others


